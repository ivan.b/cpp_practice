#!/bin/bash
# Выйдем по ошибке
set -e

echo Компилируем
g++ -o main -std=c++14 -Wall -Wextra -Wold-style-cast -finput-charset=utf-8 main.cpp

echo Запускаем
./main
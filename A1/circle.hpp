#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
public:	
	Circle(const double radius, const point_t & pos); // для pos используем ссылку, так как это - структура
	virtual double getArea() const;
	virtual rectangle_t getFrameRect() const;
	virtual void move(const point_t & pos); // смещение в заданную точку
	virtual void move(double x, double y); // смещение по осям

private:
	double radius_;
	point_t pos_;		
};

#endif // CIRCLE_HPP
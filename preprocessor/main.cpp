#include <iostream>

#define ADD_TWO(x) x + 2

int main()
{
	int i = 5;

	int j = ADD_TWO(i<<2); // = (i*4)*4

	std::cout << __LINE__ << ": " << j << "\n";

	//int x = 5;
	int k = ((i << 2) << 2); //  = (i*4)*4
	std::cout << __LINE__ << ": " << k << "\n";

	return 0;

}
